import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './web/login/login.component';
import {HomeComponent} from './web/home/home.component';
import {InicioComponent} from './web/home/pages/inicio/inicio.component';

const routes: Routes = [
  {
    path: 'mcsniffs/login',
    component: LoginComponent
  },
  {
    path: 'mcsniffs',
    component: HomeComponent,
    children: [
      {
        path: 'inicio',
        component: InicioComponent
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'mcsniffs/login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
