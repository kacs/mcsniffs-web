import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SidenavComponent } from '../web/home/sidenav/sidenav.component';
import { HomeComponent } from '../web/home/home.component';
import { HeaderComponent } from '../web/home/header/header.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { InicioComponent } from '../web/home/pages/inicio/inicio.component';
import {AppRoutingModule} from '../app-routing.module';


@NgModule({
  declarations: [
    SidenavComponent,
    HomeComponent,
    HeaderComponent,
    InicioComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    // * MATERIAL IMPORTS
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatDividerModule,
    MatListModule
  ]
})
export class HomeModule { }
